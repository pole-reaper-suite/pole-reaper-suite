-- Author: Daniel Wallen
-- Version: 0.1
-- Description: Prepares items into groups, separates the groups, regions them (with names) and adds them to Render Matrix

dofile(reaper.GetResourcePath().."\\UserPlugins\\ultraschall_api.lua")

function Msg(param)
    reaper.ShowConsoleMsg(tostring(param).."\n")
end

function SetRegion(Group_start, Group_end, name)

    if name ~= "" and Increment == "y" then
        if #Group_Positions > 999 then

            if Region_number > 999 then
                name = name.."_"..Region_number
            elseif Region_number > 99 then
                name = name.."_0"..Region_number
            elseif Region_number > 9 then
                name = name.."_00"..Region_number
            else
                name = name.."_000"..Region_number
            end

        elseif #Group_Positions > 99 then

            if Region_number > 99 then
                name = name.."_"..Region_number
            elseif Region_number > 9 then
                name = name.."_0"..Region_number
            else
                name = name.."_00"..Region_number
            end
        
        elseif #Group_Positions > 9 then

            if Region_number > 9 then
                name = name.."_"..Region_number
            else
                name = name.."_0"..Region_number
            end

        elseif not(#Group_Positions == 1) then
            name = name.."_"..Region_number
        end
    end

    if tail > 0 then
        Group_end = Group_end + tail
    end

    reaper.AddProjectMarker(0, true, Group_start, Group_end, name, -1)

    -- adds newly created region to render matrix (master)
    markeridx, regionidx = reaper.GetLastMarkerAndCurRegion(0, Group_start+0.1)
    local iRetval, bIsrgn, iPos, iRgnend, sName, iIndex, iColor = reaper.EnumProjectMarkers3(0, regionidx)
    reaper.SetRegionRenderMatrix( 0, iIndex, reaper.GetMasterTrack(0), 1)

    Region_number = Region_number+1
end

function Userinput()
    name = ""
    tail = 0
    retval, retvals_csv = reaper.GetUserInputs("Region Settings", 3, "Region name,Tail (s),Increment (y/n),extrawidth=200", ",,"..Increment)
    if retval == true then
        name, tail, Increment = retvals_csv:match("(.*),(.*),(.*)")
        if tail == "" then
            tail = 0
        else
            tail = tonumber(tail)
        end
    end
end

function GetGroupPositions()

    local n = 0
    local Item_arr = {}
    count_sel_item = reaper.CountSelectedMediaItems(0)
    selected_items = {}
    Group_Positions = {}
    First_Item = {}

    -- stores selected items start and end position in table
    for i = 1, count_sel_item do

        selected_item = reaper.GetSelectedMediaItem(0, i-1)
        item_pos_start = reaper.GetMediaItemInfo_Value(selected_item, "D_POSITION")
        item_len = reaper.GetMediaItemInfo_Value(selected_item, "D_LENGTH")
        item_pos_end = item_pos_start + item_len
        
        selected_items[i] = selected_item
        Item_arr[i] = {item_pos_start, item_pos_end, selected_item}

    end

    -- sortes table in accending order by start position
    table.sort(Item_arr, function(lhs, rhs) return lhs[1] < rhs[1] end)
    start_group = Item_arr[1][1]
    end_group = Item_arr[1][2]

    if #Item_arr <= 1 then 
        Group_Positions[1] = {start_group, end_group}
    else
        -- loop through array
        for i = 2, #Item_arr do
            -- check if items start position is less then previous items end position
            if Item_arr[i][1] < end_group then

                -- check if items end position before previous items end or not, sets timeselection to largest end_group
                if Item_arr[i][2] > end_group then
                    end_group = Item_arr[i][2]
                end

                -- check if item is the last in selection
                if i == #Item_arr then
                    n = n+1
                    Group_Positions[n] = {start_group, end_group}
                end

            else
                n = n+1
                First_Item[n] = Item_arr[i][3]
                Group_Positions[n] = {start_group, end_group}

                start_group = Item_arr[i][1]
                end_group = Item_arr[i][2]

                if i == #Item_arr then
                    n = n+1
                    Group_Positions[n] = {start_group, end_group}
                end
            end
        end
    end
    return Group_Positions
end

function Main()
    Region_number = 1

    if reaper.CountSelectedMediaItems(0) <= 0 then
        Msg("No items selected")
        return
    end

    Group_Positions = GetGroupPositions()

    Increment = "y"
    if reaper.HasExtState("RenderPrepSFX", "Increment") then
        Increment = reaper.GetExtState("RenderPrepSFX", "Increment")
    else
        Increment = "y"
    end

    Userinput()
    
    reaper.SetExtState("RenderPrepSFX", "Increment", Increment, true)

    -- we can move it inside userinput
    if retval == false then
        return
    end
    if tail == nil then
        Msg("Please enter a number into tail")
        return
    end

    if count_sel_item > 1 and tail > 0 then

        if reaper.GetToggleCommandState(40311) == 1 then
            State = 40311
        elseif reaper.GetToggleCommandState(40310) == 1 then
            State = 40310
        else
            State = 40309
        end

        reaper.Main_OnCommand(40311, 0)

        for i = 1, #First_Item do
            reaper.Main_OnCommand(40289, 0)
            reaper.SetMediaItemSelected(First_Item[i], true)
            reaper.ApplyNudge(0, 0, 0, 1, tail, false, 0)
        end

        for i = 1, #selected_items do
            reaper.SetMediaItemSelected(selected_items[i], true)
        end

        reaper.Main_OnCommand(State, 0)

    end

    Group_Positions = GetGroupPositions()
    for i = 1, #Group_Positions do
        SetRegion(Group_Positions[i][1], Group_Positions[i][2], name)
    end
end

reaper.ShowConsoleMsg("")
reaper.Undo_BeginBlock()
Main()
reaper.Undo_EndBlock( "Render prep (SFX)" , -1)
reaper.UpdateArrange()